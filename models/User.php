<?php

namespace models;

use core\Core;
use core\Utils;
use Couchbase\LookupInResult;
use task4\Rectangle;

class User
{
    protected static $tableName = 'user';

    public static function addUser($login, $password, $lastname, $firstname, $money)
    {
        \core\Core::getInstance()->db->insert(
            self::$tableName, [
                'login' => $login,
                'password' => self::hashPassword($password),
                'lastname' => $lastname,
                'firstname' => $firstname,
                'money' => $money
            ]
        );
    }

    public static function hashPassword($password)
    {
        return md5($password);
    }

    public static function updateUser($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['lastname', 'firstname', 'money']);
        \core\Core::getInstance()->db->update(self::$tableName, $updatesArray, [
            'id' => $id
        ]);
    }

    public static function isEmailExists($login)
    {
        $user = \core\Core::getInstance()->db->select('user', '*', [
            'login' => $login
        ]);
        return !empty($user);
    }

    public static function verifyUser($login, $password)
    {
        $user = \core\Core::getInstance()->db->select('user', '*', [
            'login' => $login,
            'password' => $password
        ]);
        return !empty($user);
    }

    public static function getUserByLoginAndPassword($login, $password)
    {
        $user = \core\Core::getInstance()->db->select('user', '*', [
            'login' => $login,
            'password' => self::hashPassword($password)
        ]);
        if (!empty($user)) {
            return $user[0];
        }
        return null;
    }

    public static function authenticateUser($user)
    {
        $_SESSION['user'] = $user;
    }

    public static function logoutUser()
    {
        unset($_SESSION['user']);
    }

    public static function isUserAuthenticated()
    {
        return isset($_SESSION['user']);
    }

    public static function getUserById($id)
    {
        $user = Core::getInstance()->db->select(self::$tableName, '*', [
           'id' => $id
        ]);
        return $user[0];
    }

    public static function getCurrentAuthenticatedUser()
    {
        return $_SESSION['user'];
    }

    public static function isAdmin()
    {
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 10;
    }

    public static function addMoney($id, $amount)
    {
        $user = Core::getInstance()->db->update(self::$tableName, [
            'money' => $amount
        ], [
            'id' => $id
        ]);
    }
}