<?php

namespace models;

use core\Core;
use core\Utils;
use MongoDB\Driver\Exception\CommandException;

class Cart
{
    protected static $tableName = 'cart';

    public static function addProductToCart($row)
    {
        $fieldsList = ['product_id', 'user_id', 'count', 'price'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->insert(self::$tableName, $row);
    }

    public static function getProductInCartById($id)
    {
        $cartProduct = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        return $cartProduct;
    }

    public static function deleteCartProductById($id)
    {
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function getProductsInCartByUserId($userId)
    {
        $products = Core::getInstance()->db->select(self::$tableName, '*', [
            'user_id' => $userId
        ]);
        return $products;
    }

    public static function getProductsInCartByUserIdAndStatus($userId)
    {
        $products = Core::getInstance()->db->select(self::$tableName, '*', [
            'user_id' => $userId,
            'status' => 0
        ]);
        return $products;
    }

    public static function updateCartPriceById($id, $newPrice)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'price' => $newPrice
        ], [
            'id' => $id
        ]);
    }

    public static function updateCartStatusById($id, $newStatus = 1)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'status' => $newStatus
        ], [
            'id' => $id
        ]);
    }

    public static function getProductsInCart()
    {
        $cartProducts = Core::getInstance()->db->select(self::$tableName);
        return $cartProducts;
    }
}