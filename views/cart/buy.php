<?php
/** @var array $userCartProducts */
/** @var array $userCart */
/** @var array $user */
/** @var array $errors */
?>
<style xmlns="http://www.w3.org/1999/html">
    input {
        max-width: 100px;
        display: inline-block;
    }
</style>

<h1>Оформлення замовлення</h1>
<p class="text-sm-end fs-4">Ваш баланс: <?= $user['money'] ?></p>
<?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
        <h2><?= $errors['user'] ?></h2>
    </div>
<?php endif; ?>
<form method="post" action="">
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Назва товару</th>
            <th>Ціна</th>
            <th>Кількість</th>
            <th>Сума</th>
            <th>-</th>
        </tr>
        </thead>
        <?php
        $index = 0;
        foreach ($userCartProducts as $cartProduct): ?>
            <tr>
                <td><?= $index + 1 ?></td>
                <td><a href="/product/view/<?= $cartProduct['id'] ?>"><?= $cartProduct['name'] ?></a></td>
                <td><?= $cartProduct['price'] ?></td>
                <td><input type="number" name="buyCount[<?= $cartProduct['id'] ?>]>" class="form-control"
                           value="<?= $cartProduct['buyCount'] ?>"
                           min="1"
                           max="<?= $cartProduct['count'] ?>"></td>
                <td><?= $cartProduct['price'] * $cartProduct['buyCount'] ?></td>
                <td><a href="/cart/delete/<?= $userCartProducts[$index]['cart_id'] ?>" title="Вилучити">X</a></td>
            </tr>
            <?php
            $index++;
        endforeach; ?>
        <tfoot>
        <tr>
            <th>Загальна вартість</th>
            <th></th>
            <th></th>
            <th></th>
            <th id="totalPriceOutput"><?= $userCart['totalPrice'] ?></th>
        </tfoot>
    </table>
    <?php if (!empty($userCartProducts)): ?>
        <button type="submit" class="btn btn-primary d-grid gap-2 col-1 mx-auto">Оплатити</button>
    <?php endif; ?>
</form>
<script src="/js/updatePrice.js"></script>