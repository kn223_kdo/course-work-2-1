<?php
/** @var array $user */
/** @var array $products */
/** @var array $rows */
/** @var array $rowsHistory */
?>
<style>
    a {
        text-decoration: none;
    }
</style>
<h1>Кошик</h1>
<?php if (\models\User::isAdmin()): ?>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Замовник id</th>
            <th>Товар id</th>
            <th>Кількість</th>
            <th>-</th>
        </tr>
        </thead>
        <?php
        $index = 1;
        foreach ($rows as $row): ?>
            <tr>
                <td><?= $index ?></td>
                <td><?= $row['user_id'] ?></td>
                <td><a href="/product/view/<?= $row['product_id'] ?>"><?= $row['product_id'] ?></a></td>
                <td><?= $row['count'] ?></td>
                <td><a href="/cart/delete/<?= $row['id'] ?>" title="Вилучити">X</a></td>
            </tr>
            <?php
            $index++;
        endforeach; ?>
    </table>
    <hr>
    <?php if (!empty($rowsHistory)): ?>
        <h2>Історія покупок</h2>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Замовник id</th>
                <th>Товар id</th>
                <th>Кількість</th>
            </tr>
            </thead>
            <?php
            $index = 1;
            foreach ($rowsHistory as $rowHistory): ?>
                <tr>
                    <td><?= $index ?></td>
                    <td><?= $rowHistory['user_id'] ?></td>
                    <td><a href="/product/view/<?= $rowHistory['product_id'] ?>"><?= $rowHistory['product_id'] ?></a>
                    </td>
                    <td><?= $rowHistory['count'] ?></td>
                </tr>
                <?php
                $index++;
            endforeach; ?>
        </table>
    <?php endif; ?>

<?php else: ?>
    <p class="text-sm-end fs-4">Ваш баланс: <?= $user['money'] ?></p>
    <p class="text-sm-end fs-4"><a href="/user/money" class="btn btn-primary">Поповнення</a></p>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Назва товару</th>
            <th>Ціна</th>
            <th>Кількість</th>
            <th>Сума</th>
            <th>-</th>
        </tr>
        </thead>
        <?php
        $index = 1;
        foreach ($products as $product): ?>
            <tr>
                <td><?= $index ?></td>
                <td><a href="/product/view/<?= $product['id'] ?>"><?= $product['name'] ?></a></td>
                <td><?= $product['price'] ?></td>
                <td><?= $product['buyCount'] ?></td>
                <td><?= $product['price'] * $product['buyCount'] ?></td>
                <td><a href="/cart/delete/<?= $product['cart_id'] ?>" title="Вилучити">X</a></td>
            </tr>
            <?php
            $index++;
        endforeach; ?>
        <tfoot>
        <tr>
            <th>Загальна вартість</th>
            <th></th>
            <th></th>
            <th></th>
            <th><?= $rows['totalPrice'] ?> грн</th>
            <th><a href="/cart/buy" class="btn btn-primary">Оформити замовлення</a></th>
        </tfoot>
    </table>

    <?php if (!empty($rowsHistory)): ?>
        <h2>Історія покупок</h2>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Назва товару</th>
                <th>Ціна</th>
                <th>Кількість</th>
                <th>Сума</th>
            </tr>
            </thead>
            <?php
            $index = 1;
            foreach ($rowsHistory as $rowHistory): ?>
                <tr>
                    <td><?= $index ?></td>
                    <td><a href="/product/view/<?= $rowHistory['id'] ?>"><?= $rowHistory['name'] ?></a></td>
                    <td><?= $rowHistory['price'] ?></td>
                    <td><?= $rowHistory['buyCount'] ?></td>
                    <td><?= $rowHistory['buyPrice'] ?></td>
                </tr>
                <?php
                $index++;
            endforeach; ?>
        </table>
    <?php endif; ?>
<?php endif; ?>


