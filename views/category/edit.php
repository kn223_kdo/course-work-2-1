<?php
/** @var array $category */
/** @var array $model */
/** @var array $errors */
?>

<h2>Редагування категорії</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва категорії</label>
        <input type="text" class="form-control" name="name" id="name" value="<?= $category['name'] ?>">
        <?php if (!empty($errors['name'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['name'] ?></div>
        <?php endif; ?>
    </div>
    <?php $filePath = '/files/category/'.$category['photo']; ?>
    <?php if (is_file($filePath)): ?>
        <img src="/<?= $filePath ?>" class="img-thumbnail" alt="">
    <?php else: ?>
        <img src="/static/images/no-image.jpg" class="img-thumbnail" alt="">
    <?php endif; ?>
    <div class="mb-3">
        <label for="file" class="form-label">Фото(замінити)</label>
        <input type="file" class="form-control" name="file" id="file" accept="image/jpeg">
    </div>
    <div>
        <button class="btn btn-primary">Зберегти</button>
    </div>
</form>