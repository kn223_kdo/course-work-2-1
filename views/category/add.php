<?php
/** @var array $model */
/** @var array $errors */
?>

<h2>Додавання категорії</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва категорії</label>
        <input type="text" class="form-control" name="name" id="name">
        <?php if (!empty($errors['name'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['name'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Фото</label>
        <input type="file" class="form-control" name="file" id="file" accept="image/jpeg">
    </div>
    <div>
        <button class="btn btn-primary">Додати</button>
    </div>
</form>