<?php
/** @var array $category */
/** @var array $products */
?>
<style>
    .categories-list a {
        text-decoration: none;
    }

    .categories-list .card {
        transition-duration: 0.5s;
    }

    .categories-list .card:hover {
        background-color: bisque;
        transform: scale(1.05);
    }
</style>
<h1><?= $category['name'] ?></h1>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($products as $product): ?>
        <div class="col">
            <a href="/product/view/<?= $product['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/product/' . $product['photo']; ?>
                    <?php if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $product['name'] ?></h5>
                    </div>
                    <div class="card-body">
                        <?= $product['short_description'] ?>
                    </div>
                    <?php if (\models\User::isAdmin()): ?>
                        <div class="card-body">
                            <a href="/product/edit/<?= $product['id'] ?>" class="btn btn-primary">Редагувати</a>
                            <a href="/product/delete/<?= $product['id'] ?>" class="btn btn-danger">Видалити</a>
                        </div>
                    <?php endif; ?>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
