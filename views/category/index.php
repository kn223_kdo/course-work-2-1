<?php
/** @var array $rows */

use models\User;

?>


<style>
    .categories-list a {
        text-decoration: none;
    }

    .categories-list .card {
        transition-duration: 0.5s;
    }

    .categories-list .card:hover {
        background-color: bisque;
        transform: scale(1.05);
    }
</style>
<?php if (User::isAdmin()): ?>
    <a href="/category/add" class="btn btn-success mb-3">Додати категорію</a>
<?php endif; ?>
<h2>Список категорій</h2>
<form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto" role="search" method="post">
    <input type="text" class="form-control mb-2" placeholder="Пошук..." name="name">
    <button class="btn btn-primary mb-2">Пошук</button>
</form>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($rows as $row): ?>
        <div class="col">
            <a href="/category/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/category/' . $row['photo']; ?>
                    <?php if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $row['name'] ?></h5>
                    </div>
                    <?php if (User::isAdmin()): ?>
                        <div class="card-body">
                            <a href="/category/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                            <a href="/category/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                        </div>
                    <?php endif; ?>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
