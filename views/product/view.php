<?php
/** @var array $product */
/** @var int $userId */
?>
<h1 class="h3 mb-3 fw-normal text-center"><?= $product['name'] ?></h1>
<div class="container">
    <div class="row products-list">
        <div class="col-6">
            <?php $filePath = 'files/product/' . $product['photo']; ?>
            <?php if (is_file($filePath)): ?>
                <img src="/<?= $filePath ?>" class="img-thumbnail" alt="">
            <?php else: ?>
                <img src="/static/images/no-image.jpg" class="img-thumbnail" alt="">
            <?php endif; ?>
        </div>
        <div class="col-6">
            <div class="container">
                <div class="row mb-3 mt-3">
                    <div class="col-4">
                        Ціна товару:
                    </div>
                    <div class="col-8">
                        <strong><?= $product['price'] ?> грн</strong>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-4">
                        Доступна кількість:
                    </div>
                    <div class="col-8">
                        <strong><?= $product['count'] ?> шт.</strong>
                    </div>
                </div>
                <?php if (!empty($product['short_description'])): ?>
                    <div class="row mb-3">
                        <div class="col-4">
                            Опис товару:
                        </div>
                        <div class="col-8">
                            <?= $product['description'] ?>
                        </div>
                    </div>
                <?php endif; ?>
                <form method="post" action="">
                    <div class="row mb-3">
                        <div class="col-4">
                            Кількість покупки:
                        </div>
                        <div class="col-2">
                            <div>
                                <input value="1" min="1" max="<?= $product['count'] ?>" class="form-control mb-2"
                                       type="number"
                                       name="count" id="count">
                            </div>
                            <div style="visibility: hidden">
                                <input type="text" name="user_id" value="<?= $userId ?>">
                                <input type="text" name="product_id" value="<?= $product['id'] ?>">
                            </div>
                            <div>
                                <button class="btn btn-primary mb-2">Придбати</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
