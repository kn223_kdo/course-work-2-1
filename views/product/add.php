<?php
/** @var array $model */
/** @var array $errors */
/** @var array $categories */
?>

<h2>Додавання товару</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" class="form-control" name="name" id="name">
        <?php if (!empty($errors['name'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['name'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="category_id" class="form-label">Виберіть категорію товару</label>
        <select class="form-control" name="category_id" id="category_id">
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
            <?php endforeach; ?>
        </select>
        <?php if (!empty($errors['category_id'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['category_id'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label">Ціна товару</label>
        <input type="number" class="form-control" name="price" id="price">
        <?php if (!empty($errors['price'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['price'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="count" class="form-label">Кількість одиниць товару</label>
        <input type="number" class="form-control" name="count" id="count">
        <?php if (!empty($errors['count'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['count'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="short_description" class="form-label">Короткий опис товару</label>
        <textarea class="ckeditor form-control" name="short_description" id="short_description"></textarea>
        <?php if (!empty($errors['short_description'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['short_description'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Повний опис товару</label>
        <textarea class="ckeditor form-control" name="description" id="description"></textarea>
        <?php if (!empty($errors['description'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['description'] ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Фото</label>
        <input type="file" class="form-control" name="file" id="file" accept="image/jpeg">
    </div>
    <div class="mb-3">
        <label for="visible" class="form-label">Видимість товару</label>
        <select class="form-control" name="visible" id="visible">
            <option value=1>Так</option>
            <option value="0">Ні</option>
        </select>
        <?php if (!empty($errors['visible'])): ?>
            <div id="nameHelp" class="form-text error"><?= $errors['visible'] ?></div>
        <?php endif; ?>
    </div>
    <div>
        <button class="btn btn-primary">Додати</button>
    </div>
</form>
<script src="https://cdn.ckeditor.com/ckeditor5/40.2.0/classic/ckeditor.js"></script>
<script>
    let editors = document.querySelectorAll('.ckeditor');
    for (let editor of editors) {
        ClassicEditor
            .create(editor)
            .catch(error => {
                console.error(error);
            });
    }
</script>
