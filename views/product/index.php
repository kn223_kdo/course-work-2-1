<?php
/** @var array $rows */

use models\User;

?>
<style>
    .products-list a {
        text-decoration: none;
    }

    .products-list .card {
        transition-duration: 0.5s;
    }

    .products-list .card:hover {
        background-color: bisque;
        transform: scale(1.05);
    }
</style>
<h1 class="h3 mb-3 fw-normal text-center">Список товарів</h1>
<form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto" role="search" method="post">
    <input type="text" class="form-control mb-2" placeholder="Пошук..." name="name">
    <button class="btn btn-primary mb-2">Пошук</button>
</form>
<?php if (User::isAdmin()): ?>
    <a href="/product/add" class="btn btn-success mb-3">Додати товар</a>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 products-list">
    <?php foreach ($rows as $row): ?>
        <div class="col">
            <div class="card">
                <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                    <?php $filePath = 'files/product/' . $row['photo']; ?>
                    <?php if (is_file($filePath)): ?>
                        <img src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img src="/static/images/no-image.jpg" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $row['name'] ?></h5>
                    </div>
                </a>
                <div class="card-body">
                    <?= $row['short_description'] ?>
                </div>
                <?php if (User::isAdmin()): ?>
                    <div class="card-body">
                        <a href="/product/edit/<?= $row['id'] ?>" class="btn btn-primary">Редагувати</a>
                        <a href="/product/delete/<?= $row['id'] ?>" class="btn btn-danger">Видалити</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
