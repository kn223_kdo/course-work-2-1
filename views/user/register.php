<?php
error_reporting(0);
/** @var array $errors */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Реєстрація на сайт';
?>
<h1 class="h3 mb-3 fw-normal text-center">Реєстрація користувача</h1>
<main class="form-signin w-100 m-auto">
    <form method="post" action="">
        <div class="mb-3">
            <label for="login" class="form-label">Логін</label>
            <input type="email" class="form-control" name="login" id="login" value="<?= $model['login'] ?>"
                   aria-describedby="emailHelp">
            <?php if (!empty($errors['login'])): ?>
                <div id="emailHelp" class="form-text error"><?= $errors['login'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Пароль</label>
            <input type="password" class="form-control" name="password" id="password" value="<?= $model['password'] ?>"
                   aria-describedby="passwordHelp">
            <?php if (!empty($errors['password'])): ?>
                <div id="passwordHelp" class="form-text error"><?= $errors['password'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="rep-password" class="form-label">Повторіть пароль</label>
            <input type="password" class="form-control" name="rep-password" id="rep-password"
                   value="<?= $model['rep-password'] ?>" aria-describedby="passwordHelp">
            <?php if (!empty($errors['password'])): ?>
                <div id="passwordHelp" class="form-text error"><?= $errors['password'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="firstname" class="form-label">Ім'я</label>
            <input type="text" class="form-control" name="firstname" id="firstname" value="<?= $model['firstname'] ?>"
                   aria-describedby="passwordHelp">
            <?php if (!empty($errors['firstname'])): ?>
                <div id="firstnameHelp" class="form-text error"><?= $errors['firstname'] ?></div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="lastname" class="form-label">Прізвище</label>
            <input type="text" class="form-control" name="lastname" id="lastname" value="<?= $model['lastname'] ?>"
                   aria-describedby="passwordHelp">
            <?php if (!empty($errors['lastname'])): ?>
                <div id="lastnameHelp" class="form-text error"><?= $errors['lastname'] ?></div>
            <?php endif; ?>
        </div>
        <div>
            <button class="btn btn-primary">Зареєструватися</button>
        </div>
    </form>
</main>