<h2>Поповнення рахунку</h2>
<main class="form-signin w-100 m-auto">
    <form method="post" action="">
        <div class="mb-3">
            <label for="money" class="form-label">Введіть суму поповнення</label>
            <input type="number" class="form-control" name="money" id="money">
        </div>
        <div>
            <button class="btn btn-primary">Поповнити</button>
        </div>
    </form>
</main>