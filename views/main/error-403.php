<?php
use core\Core;

Core::getInstance()->pageParams['title'] = 'Помилка 403';
?>
<div class="alert alert-danger">
    <h1>Доступ заборонено!</h1>
</div>