<?php
use core\Core;

Core::getInstance()->pageParams['title'] = 'Помилка 404';
?>
<div class="alert alert-danger">
    <h1>Помилка 404. Сторінку не знайдено!</h1>
</div>