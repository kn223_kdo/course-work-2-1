document.addEventListener('DOMContentLoaded', function () {
    var quantityInputs = document.querySelectorAll('.form-control');

    quantityInputs.forEach(function (input) {
        input.addEventListener('input', function () {
            var row = input.closest('tr');

            var price = parseFloat(row.querySelector('td:nth-child(3)').innerText);
            var count = parseInt(input.value);
            console.log(count);
            row.querySelector('td:nth-child(5)').innerText = (price * count) + ' грн';

            updateOverallTotal();
        });
    });

    function updateOverallTotal() {
        var productRows = document.querySelectorAll('.table tbody tr');

        var total = 0;

        productRows.forEach(function (row) {
            total += parseFloat(row.querySelector('td:nth-child(5)').innerText);
        });

        document.querySelector('#totalPriceOutput').innerText = total + ' грн';
    }
});