<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Cart;
use models\Category;
use models\Product;
use models\User;

class ProductController extends Controller
{
    public function indexAction()
    {
        if (Core::getInstance()->requestMethod === 'POST') {
            $searchResult = Product::getProductsByName($_POST['name']);
            return $this->render(null, [
                'rows' => $searchResult
            ]);
        }
        $rows = Product::getAllProducts();
        return $this->render(null, [
            'rows' => $rows
        ]);
    }

    public function addAction()
    {
        if (User::isAdmin()) {
            $categories = Category::getCategories();
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['name'] = trim($_POST['name']);
                $errors = [];
                if (empty($_POST['name'])) {
                    $errors['name'] = 'Назва товару не вказана!';
                }
                if (empty($_POST['category_id'])) {
                    $errors['category_id'] = 'Категорія не вибрана!';
                }
                if ($_POST['price'] <= 0) {
                    $errors['price'] = 'Ціна товару некоректно задана!';
                }
                if ($_POST['count'] <= 0) {
                    $errors['count'] = 'Кількість товару некоректно задана!';
                }
                if (empty($errors)) {
                    Product::addProduct($_POST, $_FILES['file']['tmp_name']);
                    return $this->redirect('/product');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'categories' => $categories
                    ]);
                }
            }
            return $this->render(null, [
                'categories' => $categories
            ]);
        }
    }

    public function viewAction($params)
    {
        $id = intval($params[0]);
        $product = Product::getProductById($id);
        $user = User::getCurrentAuthenticatedUser();
        if (Core::getInstance()->requestMethod == 'POST') {
            if (User::isUserAuthenticated()) {
                if (User::isAdmin()) {
                    return $this->redirect('/cart/index');
                }
                Cart::addProductToCart($_POST);
                return $this->redirect('/cart/index');
            } else {
                $this->redirect('/user/register');
            }
        }
        return $this->render(null, [
            'product' => $product,
            'userId' => $user['id']
        ]);
    }

    public function editAction($params)
    {
        $categories = Category::getCategories();
        $id = intval($params[0]);
        if (!User::isAdmin()) {
            return $this->error(403);
        }
        if ($id > 0) {
            $product = Product::getProductById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $_POST['name'] = trim($_POST['name']);
                $errors = [];
                if (empty($_POST['name'])) {
                    $errors['name'] = 'Назва Товару не вказана!';
                }
                if (empty($_POST['category_id'])) {
                    $errors['category_id'] = 'Категорія не вказана!';
                }
                if ($_POST['price'] <= 0) {
                    $errors['price'] = 'Неправильно вказана ціна!';
                }
                if ($_POST['count'] <= 0) {
                    $errors['count'] = 'Неправильно вказана кількість!';
                }
                if (empty($errors)) {
                    Product::updateProduct($id, $_POST);
                    if (!empty($_FILES['file']['tmp_name'])) {
                        Product::changePhoto($id, $_FILES['file']['tmp_name']);
                    }
                    return $this->redirect('/product/index');
                } else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'product' => $product,
                        'categories' => $categories
                    ]);
                }
            }
            return $this->render(null, [
                'product' => $product,
                'categories' => $categories
            ]);
        } else {
            return $this->error(403);
        }
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin()) {
            return $this->error(403);
        }
        if ($id > 0) {
            $product = Product::getProductById($id);
            if ($yes) {
                $filePath = 'files/product/' . $product['photo'];
                if (is_file($filePath)) {
                    unlink($filePath);
                }
                Product::deleteProductById($id);
                return $this->redirect('/product');
            }
            return $this->render(null, [
                'product' => $product
            ]);
        } else {
            return $this->error(403);
        }
    }
}