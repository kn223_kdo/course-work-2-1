<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Cart;
use models\Product;
use models\User;

class CartController extends Controller
{
    public function indexAction()
    {
        if (User::isUserAuthenticated()) {
            $products = [];
            $rowsHistory = [];
            $rows = Cart::getProductsInCart();
            if (User::isAdmin()) {
                foreach ($rows as $row) {
                    if ($row['status'] == 1) {
                        $rowsHistory [] = $row;
                        unset($rows[key($rows)]);
                    }
                }
                return $this->render(null, [
                    'rows' => $rows,
                    'rowsHistory' => $rowsHistory
                ]);
            }
            $user = User::getUserById($_SESSION['user']['id']);
            foreach ($rows as $id => $row) {
                if ($row['user_id'] === $user['id']) {
                    $product = Product::getProductById($row['product_id']);
                    if ($row['status'] == 1) {
                        $rowsHistory [] = Product::getProductById($row['product_id']);
                        $rowsHistory[$id]['buyPrice'] = $row['price'];
                        $rowsHistory[$id]['buyCount'] = $row['count'];
                        unset($rows[key($rows)]);
                    } else {
                        $product['buyCount'] = $row['count'];
                        $product['cart_id'] = $row['id'];
                        $products [] = $product;
                    }
                }
            }
            $rows['totalPrice'] = 0;
            foreach ($products as $product) {
                $rows['totalPrice'] += $product['buyCount'] * $product['price'];
            }
            return $this->render(null, [
                'user' => $user,
                'products' => $products,
                'rows' => $rows,
                'rowsHistory' => $rowsHistory
            ]);
        } else {
            $this->redirect('/user/register');
        }
    }

    public function deleteAction($params)
    {
        $user = User::getCurrentAuthenticatedUser();
        $cart = Cart::getProductInCartById($params[0]);
        if (User::isAdmin() || $user['id'] == $cart[0]['user_id']) {
            $id = intval($params[0]);
            if ($id > 0) {
                Cart::deleteCartProductById($id);
                return $this->redirect('/cart');
            }
        } else {
            $this->redirect('/');
        }
    }

    public function buyAction()
    {
        if (User::isUserAuthenticated() && !User::isAdmin()) {
            if (Core::getInstance()->requestMethod == 'POST') {
                $errors = [];
                $user = User::getUserById($_SESSION['user']['id']);
                $carts = Cart::getProductsInCartByUserIdAndStatus($user['id']);
                $products = [];
                $products['totalPrice'] = 0;
                foreach ($_POST['buyCount'] as $id => $count) {
                    $products [$id] = Product::getProductById($id);
                    $products[$id]['totalPrice'] = $products[$id]['price'] * $count;
                    $products['totalPrice'] += $products[$id]['price'] * $count;
                    foreach ($carts as $cartId => $cart) {
                        if ($products[$id]['id'] == $cart['product_id']) {
                            $carts[$cartId]['buyPrice'] = $products[$id]['totalPrice'];
                        }
                    }
                    if ($count > $products[$id]['count']) {
                        $errors['product'] = 'Немає доступних одиниць товару.';
                    }
                    $products[$id]['count'] -= $count;
                }
                if ($user['money'] <= $products['totalPrice']) {
                    $errors['user'] = 'Недостатньо коштів для покупки.';
                }
                if (!empty($errors)) {
                    return $this->render(null, [
                        'errors' => $errors
                    ]);
                } else {
                    foreach ($carts as $cartId => $cart) {
                        Cart::updateCartStatusById($cart['id']);
                        Cart::updateCartPriceById($cart['id'], $cart['buyPrice']);
                    }
                    $isFirst = true;
                    foreach ($products as $id => $product) {
                        if ($isFirst) {
                            $isFirst = false;
                            continue;
                        }
                        Product::updateProduct($id, $products[$id]);
                    }
                    $newMoney = $user['money'] - $products['totalPrice'];
                    User::updateUser($user['id'], ['money' => $newMoney]);
                    return $this->redirect('/');
                }

            }
            $userCartProducts = [];
            $userCart = [];
            $rows = Cart::getProductsInCart();
            $user = User::getUserById($_SESSION['user']['id']);
            $index = 0;
            foreach ($rows as $row) {
                if ($row['user_id'] === $user['id'] && $row['status'] === 0) {
                    $product = Product::getProductById($row['product_id']);
                    $userCartProducts [] = $product;
                    $userCartProducts [$index]['buyCount'] = $row['count'];
                    $userCartProducts [$index]['cart_id'] = $row['id'];
                    $userCart [] = $row;
                    $index++;
                }
            }
            $userCart['totalPrice'] = 0;
            foreach ($userCartProducts as $cartProduct) {
                $userCart['totalPrice'] += $cartProduct['buyCount'] * $cartProduct['price'];
            }
            return $this->render(null, [
                'userCart' => $userCart,
                'userCartProducts' => $userCartProducts,
                'user' => $user
            ]);
        }
        return $this->render();
    }
}