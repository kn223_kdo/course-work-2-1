-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 22 2023 г., 16:23
-- Версия сервера: 10.4.28-MariaDB
-- Версия PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `count`, `status`, `price`) VALUES
(57, 8, 20, 1, 1, 16499),
(58, 8, 14, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL COMMENT 'ID категорії',
  `name` varchar(255) NOT NULL COMMENT 'Назва категорії',
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `photo`) VALUES
(11, 'TCL', '6584998ba05bd.jpg'),
(12, 'LG', '658499a00c938.jpg'),
(13, 'SAMSUNG', '658499ada6e4e.jpg'),
(14, 'XIAOMI', '658499b843638.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `price` double NOT NULL,
  `count` int(11) NOT NULL,
  `short_description` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `visible` int(11) NOT NULL DEFAULT 1,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `price`, `count`, `short_description`, `description`, `visible`, `photo`) VALUES
(14, 'Телевізор Samsung UE43CU7100UXUA', 13, 16500, 5, '<p>Тип телевізора: <i>LED</i></p><p>Діагональ: <i>50\'\'</i></p><p>Вид екрану: <i>Звичайний</i></p><p>Тип підсвічування: <i>LED</i></p>', '<figure class=\"table\"><table><tbody><tr><td>Тип телевізора</td><td>&nbsp;</td><td>LED</td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR 10+</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>43\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td>4K ULTRA HD <strong>(3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>50 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>Motion Xcelerator, HLG, HDR 10+</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849b13d5400.jpg'),
(15, 'Телевізор Samsung QE50Q60CAUXUA', 13, 30500, 10, '<p>Тип телевізора: QLED</p><p>Діагональ: 50\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: Dual LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Bluetooth</td><td>&nbsp;</td><td><strong>Так</strong></td><td>&nbsp;</td></tr><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>QLED</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>Quantum HDR</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>50\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>Dual LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>4К Ultra HD (3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>50 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>100% Colour Volume with Quantum Dot, Motion Xcelerator</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849bbe527f5.jpg'),
(16, 'Телевізор Samsung UE32T4500AUXUA', 13, 9999, 7, '<p>Тип телевізора: LED</p><p>Діагональ: 32\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>32\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>HD (1366x768)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>50 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>Hyper Real, Micro Dimming Pro, HDR</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849c60c6c95.jpg'),
(17, 'Телевізор TCL 32S5400AF', 11, 8999, 15, '<p>Тип телевізора: LED</p><p>Тип матриці: VA</p><p>Діагональ: 32\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: Direct LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Bluetooth</td><td>&nbsp;</td><td><strong>Так</strong></td><td>&nbsp;</td></tr><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>Тип матриці</td><td>&nbsp;</td><td><strong>VA</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR 10</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>32\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>Direct LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>Full HD (1920x1080A)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>60 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>HDR Premium</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849e9e6b43e.jpg'),
(18, 'Телевізор TCL 55C745', 11, 34999, 4, '<p>Тип телевізора: QLED</p><p>Тип матриці: VA</p><p>Діагональ: 55\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: QLED</p>', '<figure class=\"table\"><table><tbody><tr><td>Bluetooth</td><td>&nbsp;</td><td><strong>Так</strong></td><td>&nbsp;</td></tr><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>QLED</strong></td><td>&nbsp;</td></tr><tr><td>Тип матриці</td><td>&nbsp;</td><td><strong>VA</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR 10+</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>55\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>QLED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>4К Ultra HD (3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>144 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>Dolby Vision, HDR10, PQ10, HLG, Quantum Dot, Пікова яскравість HDR: 1000 ніт, Full Array Local Dimming, IMAX Enhanced, AMD FreeSync Premium Pro, Ai-COLOR, Ai-MOTION, Ai-CLARITY, Ai-CONTRAST, Ai-HDR, Game Bar 2.0</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849f341f1bf.jpg'),
(19, 'Телевізор TCL 43P638', 11, 12999, 12, '<p>Тип телевізора: LED</p><p>Діагональ: 43\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: Direct LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR 10</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>43\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>Direct LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>4К Ultra HD (3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>60 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>HDR10, HDR HLG, HDR Dolby vision</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '65849fb0a3217.jpg'),
(20, 'Телевізор LG 43UR78006LK', 12, 16499, 4, '<p>Тип телевізора: LED</p><p>Діагональ: 43\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: Direct LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Bluetooth</td><td>&nbsp;</td><td><strong>Так</strong></td><td>&nbsp;</td></tr><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>LED</strong></td><td>&nbsp;</td></tr><tr><td>HDR</td><td>&nbsp;</td><td><strong>HDR 10</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>43\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>Direct LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>4К Ultra HD (3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>60 Гц</strong></td><td>&nbsp;</td></tr><tr><td>Технологія поліпшення зображення</td><td>&nbsp;</td><td><strong>HDR10, HLG</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '6584a02d72355.jpg'),
(21, 'Телевізор Xiaomi Mi TV Q1 75', 14, 49999, 15, '<p>Тип телевізора: QLED</p><p>Діагональ: 75\'\'</p><p>Вид екрану: Звичайний</p><p>Тип підсвічування: Direct LED</p>', '<figure class=\"table\"><table><tbody><tr><td>Bluetooth</td><td>&nbsp;</td><td><strong>Так</strong></td><td>&nbsp;</td></tr><tr><td>Тип телевізора</td><td>&nbsp;</td><td><strong>QLED</strong></td><td>&nbsp;</td></tr><tr><td>Діагональ екрану</td><td>&nbsp;</td><td><strong>75\'\'</strong></td><td>&nbsp;</td></tr><tr><td>Вид екрану</td><td>&nbsp;</td><td><strong>Звичайний</strong></td><td>&nbsp;</td></tr><tr><td>Тип підсвічування</td><td>&nbsp;</td><td><strong>Direct LED</strong></td><td>&nbsp;</td></tr><tr><td>Роздільна здатність екрану</td><td>&nbsp;</td><td><strong>4К Ultra HD (3840x2160)</strong></td><td>&nbsp;</td></tr><tr><td>Частота розгортки екрана</td><td>&nbsp;</td><td><strong>120 Гц</strong></td><td>&nbsp;</td></tr></tbody></table></figure>', 1, '6584a0bd0e9d7.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL COMMENT 'login',
  `password` varchar(255) NOT NULL COMMENT 'password',
  `lastname` varchar(255) NOT NULL COMMENT 'lname',
  `firstname` varchar(255) NOT NULL COMMENT 'fname',
  `access_level` int(11) NOT NULL DEFAULT 1,
  `money` int(11) NOT NULL DEFAULT 5000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `lastname`, `firstname`, `access_level`, `money`) VALUES
(7, 'test@gmail.com', '202cb962ac59075b964b07152d234b70', 'test', 'test', 10, 10000),
(8, '1007149@gmail.com', '202cb962ac59075b964b07152d234b70', 'user', 'user', 1, 868),
(9, '123@gmail.com', '202cb962ac59075b964b07152d234b70', '123', '123', 1, 5000);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID категорії', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
